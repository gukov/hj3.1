var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0],
    students = [],
    points = [],
    max = 0,
    maxindex = 0,
    dr = [],
    final = [];

students = studentsAndPoints.filter(function (element) {
    return typeof(element) == 'string';
});
points = studentsAndPoints.filter(function (element) {
    return typeof (element) == 'number';
});

console.log ('Список студентов:')
students.forEach(function (value, i) {
        console.log ('Студент %s набрал %d баллов', students[i], points[i]);
});

points.forEach(function (number, i) {
    if (!max || number > max) {
        max = number;
        maxindex = i;
    }
});
console.log ("Студент, набравший максимальный балл: %s  (%d баллов) ", students[maxindex], max);

points = points.map(function (value, i) {
    if (students[i] === 'Ирина Овчинникова' || students[i] === 'Александр Малов') {
        return value += 30;
    } else return value
});

students.forEach(function(value, i) {
    dr.push({name: students[i], point: points[i]});
});

dr.sort(function (a, b) {
    return b.point - a.point;
});

function getTop (amount) {
    t = dr.slice(0, amount);
    t.forEach(function(value, i) {
        final.push(t[i].name);
        final.push(t[i].point);  
    });
    return final;
};
console.log ("Топ 3:");
console.log(getTop (3));
console.log ("Топ 5:");
console.log(getTop (5));